﻿using APIProyectoIntegrador.DTO.DTOItem;
using APIProyectoIntegrador.DTO.DTOItemExistencia;
using APIProyectoIntegrador.DTO.DTOItemPartes;
using APIProyectoIntegrador.DTO.DTOLocalidad;
using APIProyectoIntegrador.DTO.DTOProductoCompleto;
using APIProyectoIntegrador.DTO.DTOProductoSolicitud;
using APIProyectoIntegrador.DTO.DTOProductoSolicitudDetalle;
using APIProyectoIntegrador.DTO.DTOSolicitudes;
using APIProyectoIntegrador.DTO.DtoUsuario;
using APIProyectoIntegrador.DTO.ItemPartes;
using APIProyectoIntegrador.DTO.Localidad;
using APIProyectoIntegrador.Models;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System.Security.Principal;

namespace APIProyectoIntegrador.Utilidades
{
    public class AutoMapperProfiles:Profile
    {
        public AutoMapperProfiles()
        {
            
            #region Solicitud
            CreateMap<Solicitud, SolicitudDTO>();
            CreateMap<CreacionSolicitudDTO, Solicitud>();
            #endregion

          
            #region ItemDTO
            CreateMap<CreacionItemDTO, Item>();
            CreateMap<Item, ItemDTO>();
            #endregion

            #region ItemExistencia
            CreateMap<CreacionItemExistenciaDTO,ItemExistencia>();
            CreateMap<ActualizarItemExistenciaDTO, ItemExistencia>();
            CreateMap<ItemExistencia,ItemExistenciaDTO>();
            #endregion

            #region ProductoSolicitud
            CreateMap<CreacionProductoSolicitudDTO,ProductoSolicitud>();
            CreateMap<ActualizarProductoSolicitudDTO, ProductoSolicitud>();
            CreateMap<ProductoSolicitud,ProductoSolicitudDTO>();
            #endregion

            #region ProductoSolicitudDetalle
            CreateMap<CreacionProductoSolicitudDetalleDTO,ProductoSolicitudDetalle>();
            CreateMap<ActualizarProductoSolicitudDetalleDTO, ProductoSolicitudDetalle>();
            CreateMap<ProductoSolicitudDetalle,ProductoSolicitudDetalleDTO>();
            #endregion

            #region ItemPartesDTO
            CreateMap<ItemParteCreacionDTO, ItemParte>();
            CreateMap<ItemParteActualizarDTO, ItemParte>();
            CreateMap<ItemParte, ItemParteDTO>();
            CreateMap<ItemParte,ItemsDTOconProductos>()
                .ForMember(x=>x.ProductoCompletos,opc=>opc.MapFrom(MapeoDTOProductos));
            #endregion

            #region ProductosCompletosDTO;
            CreateMap<ProductoCompletoCreacionDTO, PlantillaProductoCompleto>();
            CreateMap<ProductoCompletoActualizarDTO, PlantillaProductoCompleto>();
            CreateMap<PlantillaProductoCompleto,ProductoCompletosDTO>();
            CreateMap<PlantillaProductoCompleto, ProductosDTOconItems>()
                .ForMember(x=>x.ItemParte, opc=>opc.MapFrom(MapeoDTOItems));

            CreateMap<ProductoCompletoCreacionDTO, PlantillaProductoCompleto>()
                .ForMember(x => x.PlantillaProductoCompletoDetalles, opc => opc.MapFrom(MapProductoItem));

            CreateMap<ProductoCompletoActualizarDTO, PlantillaProductoCompleto>()
                .ForMember(x=>x.PlantillaProductoCompletoDetalles, opc =>opc.MapFrom(MapProductoItemUpdate));

            #endregion;
        
            #region LocalidadDTO
            CreateMap<Localidad, LocalidadDTO>();
            CreateMap<LocalidadCreacionDTO, Localidad>();
            CreateMap<LocalidadActualizacionDTO, Localidad>();
            CreateMap<AgregarLocalidadUsuarioDTO,Localidad>();
            CreateMap<Localidad, LocalidadDTOconUsuario>()
                .ForMember(x => x.Usuarios, option => option.MapFrom(MapUsuariosDTO))
                ;

            #endregion

            #region UsuarioDTO
            CreateMap<IdentityUser, UsuariosDTO>();
            #endregion

            
        }

        #region Metodos para listar los objetos

        private List<IdentityUser> MapUsuariosDTO (Localidad localidad , LocalidadDTO localidadDTO)
        {
            var resultado = new List<IdentityUser>();
            if (localidad.UsuarioLocalidad == null)
            {
                return resultado;
            }

            foreach (var item in localidad.UsuarioLocalidad)
            {
                resultado.Add(new IdentityUser()
                {
                    Id = item.UsuarioId,
                    Email = item.Usuario.Email
                });
            }
            return resultado;
        }
        private List<ProductoCompletosDTO> MapeoDTOProductos(ItemParte itemParte,ItemParteDTO itemParteDTO)
        {
            var resultado = new List<ProductoCompletosDTO>();
            if (itemParte.PlantillaProductoCompletoDetalles == null) { return resultado; }

            foreach (var producto in itemParte.PlantillaProductoCompletoDetalles)
            {
                resultado.Add(new ProductoCompletosDTO()
                {
                    Id = producto.PlantillaProductoCompletoId,
                    Nombre = producto.PlantillaProductoCompleto.Nombre,
                    Descripcion = producto.PlantillaProductoCompleto.Descripcion,
                    Estatus = producto.PlantillaProductoCompleto.Estatus
                });
            }

            return resultado;
        }

        private List<ItemParteDTO> MapeoDTOItems(PlantillaProductoCompleto plantillaProducto, ProductoCompletosDTO productoCompletos)
        {
            var resultado = new List<ItemParteDTO>();
            if (plantillaProducto.PlantillaProductoCompletoDetalles == null)
            {
                return resultado;
            }

            foreach (var item in plantillaProducto.PlantillaProductoCompletoDetalles)
            {
                resultado.Add(new ItemParteDTO()
                {
                    Id = item.ItemParteId,
                    Nombre= item.ItemParte.Nombre,
                    Tipo = item.ItemParte.Tipo,
                    Estatus= item.ItemParte.Estatus
                });
            }

            return resultado;
        }

        private List<PlantillaProductoCompletoDetalle> MapProductoItem(ProductoCompletoCreacionDTO productoCompletoCreacionDTO, PlantillaProductoCompleto plantillaProductoCompleto)
        {
            var resultado = new List<PlantillaProductoCompletoDetalle>();

            if (productoCompletoCreacionDTO.ItemParte == null)
            {
                return resultado;
            }

            foreach (var ItemParteId in productoCompletoCreacionDTO.ItemParte)
            {
                resultado.Add(new PlantillaProductoCompletoDetalle()
                {
                    ItemParteId = ItemParteId
                });
            }

            return resultado;
        }

        private List<PlantillaProductoCompletoDetalle> MapProductoItemUpdate(ProductoCompletoActualizarDTO productoCompleto, PlantillaProductoCompleto plantillaProducto)
        {
            var resultado = new List<PlantillaProductoCompletoDetalle>();

            if (productoCompleto.ItemParte == null)
            {
                return resultado;
            }

            foreach (var item in productoCompleto.ItemParte)
            {
                resultado.Add(new PlantillaProductoCompletoDetalle()
                {
                    ItemParteId = item
                });
            }
            return resultado;
        }
        #endregion

    }
}
