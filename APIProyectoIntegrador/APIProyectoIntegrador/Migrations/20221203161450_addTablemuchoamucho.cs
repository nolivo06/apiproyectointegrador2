﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace APIProyectoIntegrador.Migrations
{
    public partial class addTablemuchoamucho : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Localidades_AspNetUsers_UsuarioId",
                table: "Localidades");

            migrationBuilder.DropIndex(
                name: "IX_Localidades_UsuarioId",
                table: "Localidades");

            migrationBuilder.DropColumn(
                name: "UsuarioId",
                table: "Localidades");

            migrationBuilder.CreateTable(
                name: "UsuarioLocalidad",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UsuarioId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    LocalidadId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsuarioLocalidad", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UsuarioLocalidad_AspNetUsers_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_UsuarioLocalidad_Localidades_LocalidadId",
                        column: x => x.LocalidadId,
                        principalTable: "Localidades",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UsuarioLocalidad_LocalidadId",
                table: "UsuarioLocalidad",
                column: "LocalidadId");

            migrationBuilder.CreateIndex(
                name: "IX_UsuarioLocalidad_UsuarioId",
                table: "UsuarioLocalidad",
                column: "UsuarioId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UsuarioLocalidad");

            migrationBuilder.AddColumn<string>(
                name: "UsuarioId",
                table: "Localidades",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Localidades_UsuarioId",
                table: "Localidades",
                column: "UsuarioId");

            migrationBuilder.AddForeignKey(
                name: "FK_Localidades_AspNetUsers_UsuarioId",
                table: "Localidades",
                column: "UsuarioId",
                principalTable: "AspNetUsers",
                principalColumn: "Id");
        }
    }
}
