﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace APIProyectoIntegrador.Migrations
{
    public partial class addSolicitudProducto : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductoSolicitudDetalles_PlantillaProductoCompletos_PlantillaProductoCompletoId",
                table: "ProductoSolicitudDetalles");

            migrationBuilder.RenameColumn(
                name: "PlantillaProductoCompletoId",
                table: "ProductoSolicitudDetalles",
                newName: "ItemParteId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductoSolicitudDetalles_PlantillaProductoCompletoId",
                table: "ProductoSolicitudDetalles",
                newName: "IX_ProductoSolicitudDetalles_ItemParteId");

            migrationBuilder.AddColumn<int>(
                name: "PlantillaProductoCompletoId",
                table: "ProductoSolicitudes",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ProductoSolicitudes_PlantillaProductoCompletoId",
                table: "ProductoSolicitudes",
                column: "PlantillaProductoCompletoId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductoSolicitudDetalles_ItemPartes_ItemParteId",
                table: "ProductoSolicitudDetalles",
                column: "ItemParteId",
                principalTable: "ItemPartes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductoSolicitudes_PlantillaProductoCompletos_PlantillaProductoCompletoId",
                table: "ProductoSolicitudes",
                column: "PlantillaProductoCompletoId",
                principalTable: "PlantillaProductoCompletos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductoSolicitudDetalles_ItemPartes_ItemParteId",
                table: "ProductoSolicitudDetalles");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductoSolicitudes_PlantillaProductoCompletos_PlantillaProductoCompletoId",
                table: "ProductoSolicitudes");

            migrationBuilder.DropIndex(
                name: "IX_ProductoSolicitudes_PlantillaProductoCompletoId",
                table: "ProductoSolicitudes");

            migrationBuilder.DropColumn(
                name: "PlantillaProductoCompletoId",
                table: "ProductoSolicitudes");

            migrationBuilder.RenameColumn(
                name: "ItemParteId",
                table: "ProductoSolicitudDetalles",
                newName: "PlantillaProductoCompletoId");

            migrationBuilder.RenameIndex(
                name: "IX_ProductoSolicitudDetalles_ItemParteId",
                table: "ProductoSolicitudDetalles",
                newName: "IX_ProductoSolicitudDetalles_PlantillaProductoCompletoId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductoSolicitudDetalles_PlantillaProductoCompletos_PlantillaProductoCompletoId",
                table: "ProductoSolicitudDetalles",
                column: "PlantillaProductoCompletoId",
                principalTable: "PlantillaProductoCompletos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
