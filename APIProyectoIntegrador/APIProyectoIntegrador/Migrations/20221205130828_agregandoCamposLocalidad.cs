﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace APIProyectoIntegrador.Migrations
{
    public partial class agregandoCamposLocalidad : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Latitud",
                table: "Localidades",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Longitud",
                table: "Localidades",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Latitud",
                table: "Localidades");

            migrationBuilder.DropColumn(
                name: "Longitud",
                table: "Localidades");
        }
    }
}
