﻿namespace APIProyectoIntegrador.Models
{
    public class PlantillaProductoCompleto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioActualizacion { get; set; }

        public List<ProductoSolicitud> ProductoSolicitud { get; set; }
        public List<PlantillaProductoCompletoDetalle> PlantillaProductoCompletoDetalles { get; set; }

    }
}
