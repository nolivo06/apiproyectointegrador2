﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace APIProyectoIntegrador.Models
{
    public class UsuarioLocalidad
    {
        [Key]
        public int Id { get; set; }
        public string UsuarioId { get; set; }
        public int LocalidadId { get; set; }
        public IdentityUser Usuario { get; set; }
        public Localidad Localidad { get; set; }
    }
}
