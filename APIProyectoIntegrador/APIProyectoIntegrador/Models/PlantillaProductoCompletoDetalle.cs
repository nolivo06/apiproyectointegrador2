﻿namespace APIProyectoIntegrador.Models
{
    public class PlantillaProductoCompletoDetalle
    {
        public int ItemParteId { get; set; }
        public int PlantillaProductoCompletoId { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioActualizacion { get; set; }
        public ItemParte ItemParte { get; set; }
        public PlantillaProductoCompleto PlantillaProductoCompleto { get; set; }
    }
}
