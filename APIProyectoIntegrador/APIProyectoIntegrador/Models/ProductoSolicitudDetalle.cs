﻿using System.ComponentModel.DataAnnotations;

namespace APIProyectoIntegrador.Models
{
    public class ProductoSolicitudDetalle
    {

        public int Id { get; set; }
        public int ProductoSolicitudId { get; set; }
        public int ItemParteId { get; set; }
        public int? ItemId { get; set; }
        //public int PlantillaProductoCompletoId { get; set; }

        public bool Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public DateTime FechaEnsamblaje { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioActualizacion { get; set; }
        public string Fuente { get; set; }
        public string Referencia { get; set; }

        public ProductoSolicitud ProductoSolicitud { get; set; }
        public ItemParte ItemParte { get; set; }
        public Item Item { get; set; }
        //public PlantillaProductoCompleto PlantillaProductoCompleto { get; set; }
    }
}
