﻿namespace APIProyectoIntegrador.Models
{
    public class ProductoSolicitud
    {
        public int Id { get; set; }

        public int SolicitudId { get; set; }
        public Solicitud Solicitud { get; set; }

        public int PlantillaProductoCompletoId { get; set; }
        public PlantillaProductoCompleto PlantillaProductoCompleto { get; set; }

        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioActualizacion { get; set; }
        public string Fuente { get; set; }
        public string Referencia { get; set; }
    }
}
