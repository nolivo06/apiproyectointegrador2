﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace APIProyectoIntegrador.Models
{
    public class Localidad
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public string UsuarioCreacion { get; set; } 
        public string UsuarioActualizacion { get; set; }
        [StringLength(20)]
        public string Latitud { get; set; }
        [StringLength(20)]
        public string Longitud { get; set; }

        public List<UsuarioLocalidad> UsuarioLocalidad { get; set; }
    }
}
