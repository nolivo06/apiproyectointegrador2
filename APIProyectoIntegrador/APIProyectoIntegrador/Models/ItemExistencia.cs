﻿namespace APIProyectoIntegrador.Models
{
    public class ItemExistencia
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        public Item Item { get; set; }
        public string Lote { get; set; }
        public int Cantidad { get; set; }
        public DateTime FechaRealizada { get; set; }
        public DateTime FechaCreacion { get; set; }

        public DateTime FechaActualizacion { get; set; }
        //-----
        //-----
    }
}
