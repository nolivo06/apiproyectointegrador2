﻿namespace APIProyectoIntegrador.Models
{
    public class ItemParte
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public bool Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public string UsuarioCreacion { get; set; }
        public string UsuarioActualizacion { get; set; }

        public List<PlantillaProductoCompletoDetalle> PlantillaProductoCompletoDetalles { get; set; }
    }
}
