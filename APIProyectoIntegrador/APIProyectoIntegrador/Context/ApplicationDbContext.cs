﻿using APIProyectoIntegrador.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace APIProyectoIntegrador.Context

{
    public class ApplicationDbContext:IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<PlantillaProductoCompletoDetalle>()
                .HasKey(x => new { x.ItemParteId, x.PlantillaProductoCompletoId });

            //modelBuilder.Entity<ProductoSolicitudDetalle>()
            //    .HasKey(x => new { x.ProductoSolicitudId, x.PlantillaProductoCompletoId });

            //modelBuilder.Entity<UsuarioLocalidad>()
            //     .HasKey(x => new { x.LocalidadId, x.UsuarioId });

        }

     
        public DbSet<Localidad> Localidades { get; set; }
        public DbSet<PlantillaProductoCompletoDetalle> PlantillaProductoCompletoDetalles { get; set; }
        public DbSet<PlantillaProductoCompleto> PlantillaProductoCompletos { get; set; }
        public DbSet<ItemParte> ItemPartes { get; set; }
        public DbSet<ProductoSolicitudDetalle> ProductoSolicitudDetalles { get; set; }
        public DbSet<ProductoSolicitud> ProductoSolicitudes { get; set; }
        public DbSet<Solicitud> Solicitudes { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<ItemExistencia> ItemExistencias { get; set; }
        public DbSet<UsuarioLocalidad> UsuarioLocalidad { get; set; }

    }
}
