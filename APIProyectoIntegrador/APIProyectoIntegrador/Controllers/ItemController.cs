﻿using APIProyectoIntegrador.Context;
using APIProyectoIntegrador.DTO.DTOItem;
using APIProyectoIntegrador.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APIProyectoIntegrador.Controllers
{
    [ApiController]
    [Route("api/item")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "Admin")]
    public class ItemController:ControllerBase
    {
        private readonly ApplicationDbContext dbContext;
        private readonly IMapper mapper;

        public ItemController(ApplicationDbContext dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        [HttpGet ("listadoItems",Name = "obtenerItem")]
        public async Task<ActionResult<List<ItemDTO>>> get()
        {
            var item = await dbContext.Items.ToListAsync();

            return mapper.Map<List<ItemDTO>>(item);
        }

        [HttpGet("{id:int}/obtenerItemId")]
        public async Task<ActionResult<ItemDTO>> getById(int id)
        {
            var item = await dbContext.Items.FirstOrDefaultAsync(x=>x.Id==id);
            if (item == null)
            {
                return NotFound();
            }
            var dto = mapper.Map<ItemDTO>(item);

            return Ok(dto); 
        }

        [HttpPost("crearItem")]
        public async Task<ActionResult> Post([FromBody]CreacionItemDTO creacionItem)
        {
            var fechaCreacion = DateTime.Now;
            var item = mapper.Map<Item>(creacionItem);
            item.Estatus = true;
            item.FechaCreacion = fechaCreacion;
            dbContext.Add(item);
            await dbContext.SaveChangesAsync();

            var itemDTO = mapper.Map<ItemDTO>(item);

            return CreatedAtRoute("obtenerItem", new { id = item.Id }, itemDTO);
        }

        [HttpPut("{id:int}/actualizarItem")]
        public async Task<ActionResult> Put(int id, [FromBody] CreacionItemDTO creacionItem)
        {
            var itemId = await dbContext.Items.FirstOrDefaultAsync(x=>x.Id==id);
            if (itemId == null)
            {
                return NotFound();
            }
            var actualizarFecha = DateTime.Now;
            itemId = mapper.Map(creacionItem,itemId);
            itemId.Id = id;
            itemId.Estatus= true;
            itemId.FechaActualizacion= actualizarFecha;

            dbContext.Update(itemId);
            await dbContext.SaveChangesAsync();
            return Ok("Ha sido actualizado los datos");
        }

        [HttpDelete("{id:int}/eliminarItem")]
        public async Task<ActionResult> Delete (int id)
        {
            var itemId = await dbContext.Items.AnyAsync(x => x.Id == id);
            if (!itemId)
            {
                return NotFound();
            }

            dbContext.Remove(new Item { Id = id});
            await dbContext.SaveChangesAsync();
            return NoContent();
        }
    }
}
