﻿using APIProyectoIntegrador.Context;
using APIProyectoIntegrador.DTO.DTOItemPartes;
using APIProyectoIntegrador.DTO.ItemPartes;
using APIProyectoIntegrador.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APIProyectoIntegrador.Controllers
{
    [ApiController]
    [Route("api/itemparte")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "Admin")]
    public class ItemParteController:ControllerBase
    {
        private readonly ApplicationDbContext dbContext;
        private readonly IMapper mapper;

        public ItemParteController(ApplicationDbContext dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        [HttpGet("ListadoItemsParte", Name ="obtenerItemsPartes")]
        public async Task<ActionResult<List<ItemParteDTO>>> Get()
        {
            var item = await dbContext.ItemPartes.ToListAsync();
            return mapper.Map<List<ItemParteDTO>>(item);
        }

        [HttpGet("{id:int}/ObtenerItemParteId")]
        public async Task<ActionResult<ItemsDTOconProductos>> GetById(int id)
        {
            var item = await dbContext.ItemPartes
                       .Include(x => x.PlantillaProductoCompletoDetalles)
                       .ThenInclude(x => x.PlantillaProductoCompleto)
                       .FirstOrDefaultAsync(x=>x.Id == id);

            if (item == null)
            {
                return NotFound();
            }

            var dto = mapper.Map<ItemsDTOconProductos>(item);

            return dto;
        }


        [HttpPost("CrearItemParte")]
        public async Task<ActionResult> Post([FromBody] ItemParteCreacionDTO itemParteCreacionDTO)
        {
            var fechaCreada = DateTime.Now;
            var itemparte = mapper.Map<ItemParte>(itemParteCreacionDTO);
            itemparte.Estatus = true;
            itemparte.FechaCreacion = fechaCreada;
            dbContext.Add(itemparte);
            await dbContext.SaveChangesAsync();

            var itemparteDTO = mapper.Map<ItemParteDTO>(itemparte);

            return CreatedAtRoute("obtenerItemsPartes", new { id = itemparte.Id },itemparteDTO);
        }

        [HttpPut("{id:int}/ActualizarParteItem")]
        public async Task<ActionResult> Put([FromBody]ItemParteActualizarDTO itemParteActualizarDTO, int id)
        {
            var itemParteId = await dbContext.ItemPartes.FirstOrDefaultAsync(x=>x.Id == id);
            if (itemParteId == null)
            {
                return NotFound();
            }

            var fechaActualizar = DateTime.Now;

            itemParteId = mapper.Map(itemParteActualizarDTO,itemParteId);
            itemParteId.Id = id;
            itemParteId.Estatus = true;
            itemParteId.FechaActualizacion = fechaActualizar;

            dbContext.Update(itemParteId);
            await dbContext.SaveChangesAsync();
            return Ok("Ha sido actualizado");
        }

        [HttpDelete("{id:int}/EliminarItemParte")]
        public async Task<ActionResult> Delete ([FromHeader] int id)
        {
            var existeItem = await dbContext.ItemPartes.AnyAsync(x => x.Id == id);
            if (!existeItem)
            {
                return NotFound();
            }

            dbContext.Remove(new ItemParte() { Id = id});
            await dbContext.SaveChangesAsync();
            return NoContent();
        }
    }
}
