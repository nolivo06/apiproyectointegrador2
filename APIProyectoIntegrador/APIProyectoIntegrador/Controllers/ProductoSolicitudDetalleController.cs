﻿using APIProyectoIntegrador.Context;
using APIProyectoIntegrador.DTO.DTOProductoSolicitudDetalle;
using APIProyectoIntegrador.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APIProyectoIntegrador.Controllers
{
    [Route("api/ProductoSolicitudDetalle")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "Admin")]
    public class ProductoSolicitudDetalleController : ControllerBase
    {
        private readonly ApplicationDbContext dbContext;
        private readonly IMapper mapper;

        public ProductoSolicitudDetalleController(ApplicationDbContext dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        [HttpGet("listaProductoSolicitudDetalle" , Name ="listadoProdSoliDet")]
        public async Task<ActionResult<List<ProductoSolicitudDetalleDTO>>> Get()
        {
            var producto = await dbContext.ProductoSolicitudDetalles.ToListAsync();

            return mapper.Map<List<ProductoSolicitudDetalleDTO>>(producto);
        }

        [HttpPost("registrarProductoSolicitudDetalle")]
        public async Task<ActionResult> Post(int productoSolicitudId,int itemParteId, CreacionProductoSolicitudDetalleDTO creacionProducto)
        {
            var existeProducto = await dbContext.ProductoSolicitudes.AnyAsync(x=>x.Id == productoSolicitudId);
            if (!existeProducto) return NotFound($"No se encuentra la solicitud del producto {productoSolicitudId}");

           
            var existeItemParte = await dbContext.ItemPartes.FirstOrDefaultAsync(x=>x.Id == itemParteId);
            if (existeItemParte == null) return NotFound($"No se encuentra el item parte {itemParteId}");

            var fechaCreacion = DateTime.Now;

            var mapear = mapper.Map<ProductoSolicitudDetalle>(creacionProducto);
            mapear.ProductoSolicitudId= productoSolicitudId;
            mapear.ItemParteId = existeItemParte.Id;
            mapear.Fuente = existeItemParte.Tipo;
            mapear.Estatus = true;
            mapear.FechaCreacion= fechaCreacion;
            dbContext.Add(mapear);
            await dbContext.SaveChangesAsync();

            var dto = mapper.Map<ProductoSolicitudDetalleDTO>(mapear);

            return CreatedAtRoute("listadoProdSoliDet", new {pdId = mapear.ProductoSolicitudId, itId = mapear.ItemParteId},dto);
        }

        [HttpPut("{id:int}/editarProductoSolicitudDetalle")]
        public async Task<ActionResult> Put (int id, int productoSolicitudId, int itemId, ActualizarProductoSolicitudDetalleDTO actualizarProductoSolicitud)
        {
            var validarId = await dbContext.ProductoSolicitudDetalles.FirstOrDefaultAsync(x=>x.Id == id);
            if(validarId == null) return NotFound();

            var fechaActualizacion = DateTime.Now;
            var fechaEnsamblaje = DateTime.Now;

            validarId = mapper.Map(actualizarProductoSolicitud, validarId);
            validarId.Id = id;
            validarId.ProductoSolicitudId= productoSolicitudId;
            validarId.ItemId = itemId;
            //validarId.PlantillaProductoCompletoId = productoCompletoId;
            validarId.FechaActualizacion= fechaActualizacion;
            validarId.FechaEnsamblaje = fechaEnsamblaje;
            dbContext.Update(validarId);
            await dbContext.SaveChangesAsync();
            return Ok("Han sido actualizado los datos");
        }

        [HttpDelete("{id:int}/eliminarProductoSolicitudDetalle")]
        public async Task<ActionResult> Delete (int id)
        {
            var existeId = await dbContext.ProductoSolicitudDetalles.AnyAsync(x=>x.Id == id);
            if(!existeId) return NotFound();

            dbContext.Remove(new ProductoSolicitudDetalle { Id = id });
            await dbContext.SaveChangesAsync();
            return Ok("Han sido eliminado los datos!");
        }
    }
}
