﻿using APIProyectoIntegrador.Context;
using APIProyectoIntegrador.DTO.DTOSolicitudes;
using APIProyectoIntegrador.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APIProyectoIntegrador.Controllers
{
    [ApiController]
    [Route("api/solicitud")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "Admin")]
    public class SolicitudController:ControllerBase
    {
        private readonly ApplicationDbContext dbContext;
        private readonly IMapper mapper;

        public SolicitudController(ApplicationDbContext dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }


        [HttpGet("obtenerSolicitudes")]
        public async Task<ActionResult<List<SolicitudDTO>>> Get()
        {
            var solicitud = await dbContext.Solicitudes.ToListAsync();

            return mapper.Map<List<SolicitudDTO>>(solicitud);
        }

        [HttpGet("{id:int}/obtenerSolicitudId")]
        public async Task<ActionResult<SolicitudDTO>> GetById(int id)
        {
            var solicitud = await dbContext.Solicitudes.FirstOrDefaultAsync(x=>x.Id == id);
                                  
            if (solicitud == null)
            {
                return NotFound();
            }

            return mapper.Map<SolicitudDTO>(solicitud);
        }

        [HttpPost("registrarSolicitud")]
        public async Task<ActionResult> Post([FromBody]CreacionSolicitudDTO creacionSolicitud)
        {
           

            var fechaCreacion = DateTime.Now;
            var solicitud = mapper.Map<Solicitud>(creacionSolicitud);
            solicitud.Estatus = true;
            solicitud.FechaCreacion = fechaCreacion;
            dbContext.Add(solicitud);
            await dbContext.SaveChangesAsync();

            return Ok();
        }

        [HttpPut("{id:int}/editarSolicitud")]
        public async Task<ActionResult> put(int id,CreacionSolicitudDTO actualizarSolicitud)
        {
            var solicitudId = await dbContext.Solicitudes.FirstOrDefaultAsync(x=>x.Id == id);

            if (solicitudId == null)
            {
                return NotFound($"El id: {id} no existe registrado");
            }

            var fechaActualizacion = DateTime.UtcNow;
            solicitudId = mapper.Map(actualizarSolicitud,solicitudId);
            solicitudId.Id = id;
            solicitudId.FechaActualizacion = fechaActualizacion;
            dbContext.Update(solicitudId);
            await dbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var solicitudId = await dbContext.Solicitudes.AnyAsync(x => x.Id == id);

            if (!solicitudId)
            {
                return NotFound($"El id: {id} no existe registrado");
            }

            dbContext.Remove(new Solicitud() { Id= id });
            await dbContext.SaveChangesAsync();
            return Ok($"Ha sido eliminado el registro con el id: {id}");
        }
    }
}
