﻿using APIProyectoIntegrador.Context;
using APIProyectoIntegrador.DTO.DtoUsuario;
using APIProyectoIntegrador.DTO.DTOUsuario;
using APIProyectoIntegrador.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Diagnostics.SymbolStore;
using System.IdentityModel.Tokens.Jwt;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Text;

namespace APIProyectoIntegrador.Controllers
{
    [Route("api/cuentas")]
    [ApiController]
    public class CuentasController : ControllerBase
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly IConfiguration configuration;
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly ApplicationDbContext dbContext;
        private readonly IMapper mapper;

        public CuentasController(UserManager<IdentityUser> userManager, IConfiguration configuration,
                                SignInManager<IdentityUser> signInManager, ApplicationDbContext dbContext,
                                IMapper mapper)
        {
            this.userManager = userManager;
            this.configuration = configuration;
            this.signInManager = signInManager;
            this.dbContext = dbContext;
            this.mapper = mapper;
        }
        

        [HttpGet("ListadoUsuarios")]
        public async Task<ActionResult<List<UsuariosDTO>>> get()
        {
            var listado = await userManager.Users.Select(x => new UsuariosDTO
            {
                Id= x.Id,
                Email= x.Email
            }).ToListAsync();

            if (listado.Count < 0)
            {
                
                return NotFound();
            }
            else
            {
                return listado;
            }
        }

        [HttpPost("Registrar")]
        public async Task<ActionResult<RespuestaAutenticacion>> Registrar([FromBody]RegistroUsuarioDTO registro)
        {
           
            var existeUsuario = await userManager.Users.AnyAsync(x=>x.Email == registro.Email);

            if (existeUsuario)
            {
                return BadRequest($"Ya existe una cuenta de correo {registro.Email} registrado");
            }


            var usuario = new IdentityUser
            {
                UserName = registro.Email,
                Email = registro.Email 
            };
        
            var resultado = await userManager.CreateAsync(usuario, registro.Clave);

            if (resultado.Succeeded)
            {
                return Ok("Usuario creado correctamente");
            }
            else
            {
                return BadRequest(resultado.Errors);
            }



        }

        [HttpPost("CambiarClaveOlvidada")]
        public async Task<ActionResult<RespuestaAutenticacion>> CambiarClaveOlvidada([FromBody]CambiarClaveOlvidadaDTO credencialesUsuario)
        {
            var user = await userManager.FindByEmailAsync(credencialesUsuario.Email);
            
            if (user == null) return NotFound();

            if (string.Compare(credencialesUsuario.ClaveNueva, credencialesUsuario.ConfirmarClaveNueva) != 0)
                return BadRequest($"La clave no coinciden, favor revisar la clave actual");

            var token = await userManager.GeneratePasswordResetTokenAsync(user);

            var resultado = await userManager.ResetPasswordAsync(user, token,credencialesUsuario.ClaveNueva);
            if(!resultado.Succeeded)
            {
                return BadRequest("No se ha podido cambiar la clave, revisa el usuario!");
            }


            return Ok("La clave ha sido cambiada satisfactoriamente");
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "Admin")]
        [HttpPost("CambiarClave")]
        public async Task<ActionResult> CambiarClave([FromBody] CambiarClaveDTO cambiarClave)
        {
            var user = await userManager.FindByEmailAsync(cambiarClave.Email);

            if (user == null) return NotFound();

            if (string.Compare(cambiarClave.ClaveNueva, cambiarClave.ConfirmarClaveNueva) != 0)
                return BadRequest("Las claves no coinciden! , Favor revisar");

            var result = await userManager.ChangePasswordAsync(user,cambiarClave.ClaveAnterior,cambiarClave.ClaveNueva);

            if (!result.Succeeded)
                return BadRequest("Ha ocurrido un error para cambiar la clave, favor revisar");

            return Ok("Ha sido cambiada la clave");
        }

        [HttpPost("IniciarSession")]
        public async Task<ActionResult<RespuestaAutenticacion>> Login([FromBody]CredencialesUsuario credencialesUsuario)
        {
            var usuario = await signInManager.PasswordSignInAsync(credencialesUsuario.Email,
                                credencialesUsuario.Clave, isPersistent:false,lockoutOnFailure:false);
            if (usuario.Succeeded)
            {
                return await construirToken(credencialesUsuario);
            }
            else
            {
                return BadRequest("Credenciales Incorrectas");
            }
        }

       
        private async Task<RespuestaAutenticacion> construirToken(CredencialesUsuario credencialesUsuario)
        {
            var claims = new List<Claim>() 
            { 
                new Claim("Admin", credencialesUsuario.Email)
            };

            var usuario = await userManager.FindByEmailAsync(credencialesUsuario.Email);
            var claimsdb = await userManager.GetClaimsAsync(usuario);

            claims.AddRange(claimsdb);

            var llave = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["key"]));

            var creds = new SigningCredentials(llave, SecurityAlgorithms.HmacSha256);

            var expiracion = DateTime.UtcNow.AddMinutes(30);

            var token = new JwtSecurityToken(issuer:null, audience:null,claims:claims,expires:expiracion, signingCredentials:creds);

            return new RespuestaAutenticacion()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiracion = expiracion
                
            };
        }

    }
}
