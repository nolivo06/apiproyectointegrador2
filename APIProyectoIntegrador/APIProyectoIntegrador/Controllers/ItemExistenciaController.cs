﻿using APIProyectoIntegrador.Context;
using APIProyectoIntegrador.DTO.DTOItemExistencia;
using APIProyectoIntegrador.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APIProyectoIntegrador.Controllers
{
    [ApiController]
    [Route("api/item/{itemId:int}/itemExistencia")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "Admin")]
    public class ItemExistenciaController:ControllerBase
    {
        private readonly ApplicationDbContext dbContext;
        private readonly IMapper mapper;

        public ItemExistenciaController(ApplicationDbContext dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        [HttpGet("obtenerItemExistencias", Name = "obtenerItemExistencias")]
        public async Task<ActionResult<List<ItemExistenciaDTO>>> get(int itemId)
        {
            var existeItem = await dbContext.Items.AnyAsync(x=>x.Id == itemId);

            if (!existeItem)
            {
                return NotFound($"No se encuentra el item con el codigo {itemId}");
            }

            var queryable = dbContext.ItemExistencias.Where(x=>x.ItemId == itemId).AsQueryable();

            return mapper.Map<List<ItemExistenciaDTO>>(queryable);

        }

        [HttpPost("crearItemExistencia")]
        public async Task<ActionResult> Post(int itemId, [FromBody] CreacionItemExistenciaDTO itemExistenciaDTO)
        {
            var existeItem = await dbContext.Items.AnyAsync(x=>x.Id == itemId);

            if (!existeItem) { return NotFound(); }

            var fechaCreacion = DateTime.Now;
            var fechaRealizada = DateTime.Now;

            var itemExistencia = mapper.Map<ItemExistencia>(itemExistenciaDTO);
            itemExistencia.ItemId= itemId;
            itemExistencia.FechaCreacion = fechaCreacion;
            itemExistencia.FechaRealizada = fechaRealizada;
            dbContext.Add(itemExistencia);
            await dbContext.SaveChangesAsync();
            var existenciaDTO = mapper.Map<ItemExistenciaDTO>(itemExistencia);
            return CreatedAtRoute("obtenerItemExistencias", new {id=itemExistencia.Id,itemId=itemExistencia.ItemId}, existenciaDTO);

        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult> Put(int itemId, int id, [FromBody] ActualizarItemExistenciaDTO itemExistenciaDTO)
        {
            var existeItem = await dbContext.Items.AnyAsync(x => x.Id == itemId);

            if (!existeItem) { return NotFound(); }

            var existenciaItemId = await dbContext.ItemExistencias.FirstOrDefaultAsync(x => x.Id == id);
            if (existenciaItemId == null) { return NotFound(); }

            var fechaActualizacion = DateTime.Now;

            existenciaItemId = mapper.Map(itemExistenciaDTO, existenciaItemId);
            existenciaItemId.ItemId= itemId;
            existenciaItemId.Id = id;
            existenciaItemId.FechaActualizacion= fechaActualizacion;
            dbContext.Update(existenciaItemId);
            await dbContext.SaveChangesAsync();

            return Ok($"Ha sido actualizado la existencia del item {itemId}");
        }


        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int itemId, int id)
        {
            var existeItem = await dbContext.Items.AnyAsync(x => x.Id == itemId);

            if (!existeItem) { return NotFound(); }

            var existenciaItemId = await dbContext.ItemExistencias.AnyAsync(x => x.Id == id);
            if (!existenciaItemId) { return NotFound(); }

            dbContext.Remove(new ItemExistencia {Id = id});
            await dbContext.SaveChangesAsync();
            return Ok();
        }
    }
}
