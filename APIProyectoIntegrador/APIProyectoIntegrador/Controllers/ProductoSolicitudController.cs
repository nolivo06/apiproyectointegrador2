﻿using APIProyectoIntegrador.Context;
using APIProyectoIntegrador.DTO.DTOProductoSolicitud;
using APIProyectoIntegrador.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Formats.Asn1;

namespace APIProyectoIntegrador.Controllers
{
    [ApiController]
    [Route("api/{solicitudId:int}/productoSolicitud")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "Admin")]
    public class ProductoSolicitudController:ControllerBase
    {
        private readonly ApplicationDbContext dbContext;
        private readonly IMapper mapper;

        public ProductoSolicitudController(ApplicationDbContext dbContext,
                            IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        [HttpGet("obtenerProductoSolicitud", Name = "listadoProductoSolicitud")]
        public async Task<ActionResult<List<ProductoSolicitudDTO>>> getByProducto(int solicitudId)
        {
            var existeSolicitud = await dbContext.Solicitudes.AnyAsync(x=>x.Id == solicitudId);

            if (!existeSolicitud)
            {
                return NotFound();
            }

            var productoSolicitud = dbContext.ProductoSolicitudes.Where(x => x.SolicitudId== solicitudId).AsQueryable();
            return  mapper.Map<List<ProductoSolicitudDTO>>(productoSolicitud);
        }

        [HttpPost("crearProductoSolicitud")]
        public async Task<ActionResult> Post (int solicitudId,int productoCompletoId,[FromBody]CreacionProductoSolicitudDTO productoSolicitudDTO)
        {
            var existeSolicitud = await dbContext.Solicitudes.AnyAsync(x=>x.Id == solicitudId);
            if (!existeSolicitud)
            {
                return NotFound($"No se encuentra la solicitud con el codigo {solicitudId}");
            }

            var existeProductoCompleto = await dbContext.PlantillaProductoCompletos.AnyAsync(x=>x.Id == productoCompletoId);
            if (!existeProductoCompleto) return NotFound($"No se encuentra productos con el codigo: {productoCompletoId}");

            var productoCompleto = await dbContext.PlantillaProductoCompletos.FirstOrDefaultAsync();

            var fechaCreacion = DateTime.Now;

            var DTOproductoSolicitud = mapper.Map<ProductoSolicitud>(productoSolicitudDTO);
            DTOproductoSolicitud.SolicitudId = solicitudId;
            DTOproductoSolicitud.PlantillaProductoCompletoId = productoCompletoId;
            DTOproductoSolicitud.Nombre = productoCompleto.Nombre;
            DTOproductoSolicitud.Estatus = true;
            DTOproductoSolicitud.FechaCreacion = fechaCreacion;
            dbContext.Add(DTOproductoSolicitud);
            await dbContext.SaveChangesAsync();

            var productoSolicitud = mapper.Map<ProductoSolicitudDTO>(DTOproductoSolicitud);

            return Ok();
            //return CreatedAtRoute("listadoProductoSolicitud", new { id = DTOproductoSolicitud.Id, idSolicitud = DTOproductoSolicitud.SolicitudId}, productoSolicitud);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult> Put(int solicitudId, int id, [FromBody] ActualizarProductoSolicitudDTO actualizarProductoSolicitud)
        {
            var existeSolicitud = await dbContext.Solicitudes.AnyAsync(x => x.Id == solicitudId);
            if (!existeSolicitud)
            {
                return NotFound($"No se encuentra la solicitud con el codigo {solicitudId}");
            }

            var solicitudProducto = await dbContext.ProductoSolicitudes.FirstOrDefaultAsync(x=>x.Id == id);
            if (solicitudProducto == null) { return NotFound(); }

            var fechaActualizacion = DateTime.Now;

            solicitudProducto = mapper.Map(actualizarProductoSolicitud,solicitudProducto);
            solicitudProducto.SolicitudId= solicitudId;
            solicitudProducto.Id = id;
            solicitudProducto.FechaActualizacion= fechaActualizacion;
            dbContext.Update(solicitudProducto);
            await dbContext.SaveChangesAsync();

            return Ok();
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int solicitudId, int id)
        {
            var existeSolicitud = await dbContext.Solicitudes.AnyAsync(x => x.Id == solicitudId);
            if (!existeSolicitud)
            {
                return NotFound($"No se encuentra la solicitud con el codigo {solicitudId}");
            }

            var existeId = await dbContext.ProductoSolicitudes.AnyAsync(x=>x.Id == id);
            if (!existeId) { return NotFound(); }

            dbContext.Remove(new ProductoSolicitud { Id = id});
            await dbContext.SaveChangesAsync(); return Ok();
        }
    }
}
