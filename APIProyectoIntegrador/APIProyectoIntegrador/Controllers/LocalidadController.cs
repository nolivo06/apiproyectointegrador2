﻿using APIProyectoIntegrador.Context;
using APIProyectoIntegrador.DTO.DTOLocalidad;
using APIProyectoIntegrador.DTO.DtoUsuario;
using APIProyectoIntegrador.DTO.Localidad;
using APIProyectoIntegrador.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APIProyectoIntegrador.Controllers
{
    [ApiController]
    [Route("api/localidad")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "Admin")]
    public class LocalidadController : ControllerBase
    {
        private readonly ApplicationDbContext dbContext;
        private readonly IMapper mapper;
        private readonly UserManager<IdentityUser> userManager;

        public LocalidadController(ApplicationDbContext dbContext, IMapper mapper,
            UserManager<IdentityUser> userManager)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
            this.userManager = userManager;
        }



        [HttpGet("ListadoLocalidades", Name = "Listado")]
        public async Task<ActionResult<List<LocalidadDTO>>> get()
        {
            var localidad = await dbContext.Localidades.ToListAsync();

            return mapper.Map<List<LocalidadDTO>>(localidad);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<LocalidadDTO>> GetById(int id)
        {
            var localidad = await dbContext.Localidades.FirstOrDefaultAsync(x => x.Id == id);

            if (localidad == null) { return NotFound(); }

            var localidadDTO = mapper.Map<LocalidadDTO>(localidad);

            return localidadDTO;
        }

        [HttpGet("{id:int}/ListadoLocalidadUsuario")]
        public async Task<ActionResult<LocalidadDTOconUsuario>> GetLocalidadUsuario(int id)
        {
            var localidadUsuario = await dbContext.Localidades
                .Include(x => x.UsuarioLocalidad)
                .ThenInclude(x=>x.Usuario)
                .FirstOrDefaultAsync(x=>x.Id == id);

            if (localidadUsuario == null)
            {
                return NotFound();
            }

            return mapper.Map<LocalidadDTOconUsuario>(localidadUsuario);
        }

        [HttpPost("AgregarUsuarioLocalidad")]
        public async Task<ActionResult> AddLocalidad(int localidadid, string usuarioId)
        {

            var existeLocalidad = await dbContext.Localidades.AnyAsync(x => x.Id == localidadid);
            if (!existeLocalidad)
            {
                return BadRequest("No existe las localidades enviadas!");
            }


            var user = await userManager.Users.FirstOrDefaultAsync(x => x.Id == usuarioId);
            if (user == null) return NotFound($"No existe usuario registrado");

            var localidad = new UsuarioLocalidad();

            localidad.LocalidadId = localidadid;
            localidad.UsuarioId = usuarioId;

            dbContext.Add(localidad);
            await dbContext.SaveChangesAsync();

            return Ok("Ha sido agregado de manera satisfactoria");

        }

        [HttpPost("CrearLocalidad")]
        public async Task<ActionResult> Post([FromBody] LocalidadCreacionDTO localidadCreacion)
        {
            var existeLocalidad = await dbContext.Localidades.AnyAsync(x => x.Nombre == localidadCreacion.Nombre);

            if (existeLocalidad)
            {
                return BadRequest($"La localidad {localidadCreacion.Nombre} ya existe registrada");
            }

            var fechaCreacion = DateTime.Now;

            var localidad = mapper.Map<Localidad>(localidadCreacion);
            localidad.FechaCreacion = fechaCreacion;
            localidad.Estatus = "Activo";
            dbContext.Add(localidad);
            await dbContext.SaveChangesAsync();
            var localidadDTO = mapper.Map<LocalidadDTO>(localidad);

            return CreatedAtRoute("Listado", new { id = localidad.Id }, localidadDTO);
        }

        [HttpPut("{id:int}/ActualizarLocalidad")]
        public async Task<ActionResult> Put(int id, LocalidadActualizacionDTO actualizacionDTO)
        {
            var localidadId = await dbContext.Localidades.FirstOrDefaultAsync(x => x.Id == id);
            if (localidadId == null)
            {
                return NotFound();
            }

            var existeLocalidad = await dbContext.Localidades.AnyAsync(x => x.Nombre == actualizacionDTO.Nombre);

            if (existeLocalidad)
            {
                return BadRequest($"La localidad {actualizacionDTO.Nombre} ya existe registrada");
            }

            var fechaActualizada = DateTime.Now;

            localidadId = mapper.Map(actualizacionDTO, localidadId);
            localidadId.Id = id;
            localidadId.FechaActualizacion= fechaActualizada;
            dbContext.Update(localidadId);
            await dbContext.SaveChangesAsync();


            return Ok("Ha sido actualizado correctamente");
        }

        [HttpDelete("{id:int}/EliminarLocalidad")]
        public async Task<ActionResult> Delete (int id)
        {
            var existe = await dbContext.Localidades.AnyAsync(x => x.Id == id);
            if (!existe)
            {
                return NotFound();
            }

            dbContext.Remove(new Localidad() { Id=id});
            await dbContext.SaveChangesAsync();
            return Ok($"Ha sido eliminado la localidad con el codigo {id}");
        }


    }
}
