﻿using APIProyectoIntegrador.Context;
using APIProyectoIntegrador.DTO.DTOProductoCompleto;
using APIProyectoIntegrador.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APIProyectoIntegrador.Controllers
{
    [ApiController]
    [Route("api/productoCompleto")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "Admin")]
    public class ProductoCompletoController:ControllerBase
    {
        private readonly ApplicationDbContext dbContext;
        private readonly IMapper mapper;

        public ProductoCompletoController(ApplicationDbContext dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        [HttpGet("ObtenerProducto", Name = "obtenerProducto")]
        public async Task<ActionResult<List<ProductoCompletosDTO>>> get()
        {
            var producto = await dbContext.PlantillaProductoCompletos.ToListAsync();

            return mapper.Map<List<ProductoCompletosDTO>>(producto);
        }

        [HttpGet("{id:int}/ObtenerProductoId")]
        public async Task<ActionResult<ProductosDTOconItems>> getProducto(int id)
        {
            var producto = await dbContext.PlantillaProductoCompletos
                           .Include(x => x.PlantillaProductoCompletoDetalles)
                           .ThenInclude(x => x.ItemParte)
                           .FirstOrDefaultAsync(x => x.Id == id);

            if (producto == null)
            {
                return NotFound();
            }

            return mapper.Map<ProductosDTOconItems>(producto);
        }

        [HttpPost("CrearProducto")]
        public async Task<ActionResult> Post([FromBody] ProductoCompletoCreacionDTO productoCompletoCreacionDTO)
        {
            if (productoCompletoCreacionDTO.ItemParte == null)
            {
                return BadRequest("No se puede crear un producto sin Items");
            }

            var itempartes = await dbContext.ItemPartes
                .Where(x => productoCompletoCreacionDTO.ItemParte.Contains(x.Id)).Select(x=>x.Id).ToListAsync();

            if (productoCompletoCreacionDTO.ItemParte.Count != itempartes.Count)
            {
                return BadRequest("No existen los Items enviados");
            }

            var fechaCreacion = DateTime.Now;

            var producto = mapper.Map<PlantillaProductoCompleto>(productoCompletoCreacionDTO);
            producto.Estatus = true;
            producto.FechaCreacion = fechaCreacion;
            dbContext.Add(producto);
            await dbContext.SaveChangesAsync();

            var productoDTO = mapper.Map<ProductoCompletosDTO>(producto);

            return CreatedAtRoute("obtenerProducto", new { id = producto.Id }, productoDTO);
        }

        [HttpPut("{id:int}/EditarProducto")]
        public async Task<ActionResult> Put(int id, ProductoCompletoActualizarDTO productoCompleto)
        {
            var productoId = await dbContext.PlantillaProductoCompletos
                .Include(x => x.PlantillaProductoCompletoDetalles)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (productoId == null) { return NotFound(); }

            var fechaActualizacion = DateTime.Now;

            productoId = mapper.Map(productoCompleto, productoId);
            productoId.Estatus = true;
            productoId.FechaActualizacion = fechaActualizacion;
            dbContext.Update(productoId);
            await dbContext.SaveChangesAsync();

            var productoDTO = mapper.Map<ProductoCompletosDTO>(productoId);

            return CreatedAtRoute("obtenerProducto", new {id = productoId.Id}, productoDTO);

        }

        [HttpDelete("{id:int}/EliminarProducto")]
        public async Task<ActionResult> Delete ( int id)
        {
            var existeProducto = await dbContext.PlantillaProductoCompletos.AnyAsync(x=>x.Id == id);

            if (!existeProducto)
            {
                return NotFound();
            }

            dbContext.Remove(new PlantillaProductoCompleto() { Id = id});
            await dbContext.SaveChangesAsync();
            return Ok($"Ha sido eliminado el producto con el codigo: {id} ");

        }
    }
}
