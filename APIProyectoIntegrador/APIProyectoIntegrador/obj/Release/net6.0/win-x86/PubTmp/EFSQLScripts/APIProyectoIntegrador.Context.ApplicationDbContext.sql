﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE TABLE [AspNetRoles] (
        [Id] nvarchar(450) NOT NULL,
        [Name] nvarchar(256) NULL,
        [NormalizedName] nvarchar(256) NULL,
        [ConcurrencyStamp] nvarchar(max) NULL,
        CONSTRAINT [PK_AspNetRoles] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE TABLE [AspNetUsers] (
        [Id] nvarchar(450) NOT NULL,
        [UserName] nvarchar(256) NULL,
        [NormalizedUserName] nvarchar(256) NULL,
        [Email] nvarchar(256) NULL,
        [NormalizedEmail] nvarchar(256) NULL,
        [EmailConfirmed] bit NOT NULL,
        [PasswordHash] nvarchar(max) NULL,
        [SecurityStamp] nvarchar(max) NULL,
        [ConcurrencyStamp] nvarchar(max) NULL,
        [PhoneNumber] nvarchar(max) NULL,
        [PhoneNumberConfirmed] bit NOT NULL,
        [TwoFactorEnabled] bit NOT NULL,
        [LockoutEnd] datetimeoffset NULL,
        [LockoutEnabled] bit NOT NULL,
        [AccessFailedCount] int NOT NULL,
        CONSTRAINT [PK_AspNetUsers] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE TABLE [ItemPartes] (
        [Id] int NOT NULL IDENTITY,
        [Nombre] nvarchar(max) NULL,
        [Tipo] nvarchar(max) NULL,
        [Estatus] bit NOT NULL,
        [FechaCreacion] datetime2 NOT NULL,
        [FechaActualizacion] datetime2 NOT NULL,
        [UsuarioCreacion] nvarchar(max) NULL,
        [UsuarioActualizacion] nvarchar(max) NULL,
        CONSTRAINT [PK_ItemPartes] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE TABLE [Items] (
        [Id] int NOT NULL IDENTITY,
        [Nombre] nvarchar(max) NULL,
        [Tipo] nvarchar(max) NULL,
        [Estatus] bit NOT NULL,
        [FechaCreacion] datetime2 NOT NULL,
        [FechaActualizacion] datetime2 NOT NULL,
        [UsuarioCreacion] nvarchar(max) NULL,
        [UsuarioActualizacion] nvarchar(max) NULL,
        [Fuente] nvarchar(max) NULL,
        [Referencia] nvarchar(max) NULL,
        CONSTRAINT [PK_Items] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE TABLE [PlantillaProductoCompletos] (
        [Id] int NOT NULL IDENTITY,
        [Nombre] nvarchar(max) NULL,
        [Descripcion] nvarchar(max) NULL,
        [Estatus] bit NOT NULL,
        [FechaCreacion] datetime2 NOT NULL,
        [FechaActualizacion] datetime2 NOT NULL,
        [UsuarioCreacion] nvarchar(max) NULL,
        [UsuarioActualizacion] nvarchar(max) NULL,
        CONSTRAINT [PK_PlantillaProductoCompletos] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE TABLE [Solicitudes] (
        [Id] int NOT NULL IDENTITY,
        [Nombre] nvarchar(max) NULL,
        [Descripcion] nvarchar(max) NULL,
        [Estatus] bit NOT NULL,
        [FechaCreacion] datetime2 NOT NULL,
        [FechaActualizacion] datetime2 NOT NULL,
        [UsuarioCreacion] nvarchar(max) NULL,
        [UsuarioActualizacion] nvarchar(max) NULL,
        CONSTRAINT [PK_Solicitudes] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE TABLE [AspNetRoleClaims] (
        [Id] int NOT NULL IDENTITY,
        [RoleId] nvarchar(450) NOT NULL,
        [ClaimType] nvarchar(max) NULL,
        [ClaimValue] nvarchar(max) NULL,
        CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE TABLE [AspNetUserClaims] (
        [Id] int NOT NULL IDENTITY,
        [UserId] nvarchar(450) NOT NULL,
        [ClaimType] nvarchar(max) NULL,
        [ClaimValue] nvarchar(max) NULL,
        CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE TABLE [AspNetUserLogins] (
        [LoginProvider] nvarchar(450) NOT NULL,
        [ProviderKey] nvarchar(450) NOT NULL,
        [ProviderDisplayName] nvarchar(max) NULL,
        [UserId] nvarchar(450) NOT NULL,
        CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY ([LoginProvider], [ProviderKey]),
        CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE TABLE [AspNetUserRoles] (
        [UserId] nvarchar(450) NOT NULL,
        [RoleId] nvarchar(450) NOT NULL,
        CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY ([UserId], [RoleId]),
        CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE TABLE [AspNetUserTokens] (
        [UserId] nvarchar(450) NOT NULL,
        [LoginProvider] nvarchar(450) NOT NULL,
        [Name] nvarchar(450) NOT NULL,
        [Value] nvarchar(max) NULL,
        CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY ([UserId], [LoginProvider], [Name]),
        CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE TABLE [Localidades] (
        [Id] int NOT NULL IDENTITY,
        [Nombre] nvarchar(max) NULL,
        [Estatus] nvarchar(max) NULL,
        [FechaCreacion] datetime2 NOT NULL,
        [FechaActualizacion] datetime2 NOT NULL,
        [UsuarioCreacion] nvarchar(max) NULL,
        [UsuarioActualizacion] nvarchar(max) NULL,
        [UsuarioId] nvarchar(450) NULL,
        CONSTRAINT [PK_Localidades] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Localidades_AspNetUsers_UsuarioId] FOREIGN KEY ([UsuarioId]) REFERENCES [AspNetUsers] ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE TABLE [ItemExistencias] (
        [Id] int NOT NULL IDENTITY,
        [ItemId] int NOT NULL,
        [Lote] nvarchar(max) NULL,
        [Cantidad] int NOT NULL,
        [FechaRealizada] datetime2 NOT NULL,
        [FechaCreacion] datetime2 NOT NULL,
        [FechaActualizacion] datetime2 NOT NULL,
        CONSTRAINT [PK_ItemExistencias] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_ItemExistencias_Items_ItemId] FOREIGN KEY ([ItemId]) REFERENCES [Items] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE TABLE [PlantillaProductoCompletoDetalles] (
        [ItemParteId] int NOT NULL,
        [PlantillaProductoCompletoId] int NOT NULL,
        [FechaCreacion] datetime2 NOT NULL,
        [FechaActualizacion] datetime2 NOT NULL,
        [UsuarioCreacion] nvarchar(max) NULL,
        [UsuarioActualizacion] nvarchar(max) NULL,
        CONSTRAINT [PK_PlantillaProductoCompletoDetalles] PRIMARY KEY ([ItemParteId], [PlantillaProductoCompletoId]),
        CONSTRAINT [FK_PlantillaProductoCompletoDetalles_ItemPartes_ItemParteId] FOREIGN KEY ([ItemParteId]) REFERENCES [ItemPartes] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_PlantillaProductoCompletoDetalles_PlantillaProductoCompletos_PlantillaProductoCompletoId] FOREIGN KEY ([PlantillaProductoCompletoId]) REFERENCES [PlantillaProductoCompletos] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE TABLE [ProductoSolicitudes] (
        [Id] int NOT NULL IDENTITY,
        [SolicitudId] int NOT NULL,
        [Nombre] nvarchar(max) NULL,
        [Descripcion] nvarchar(max) NULL,
        [Estatus] bit NOT NULL,
        [FechaCreacion] datetime2 NOT NULL,
        [FechaActualizacion] datetime2 NOT NULL,
        [UsuarioCreacion] nvarchar(max) NULL,
        [UsuarioActualizacion] nvarchar(max) NULL,
        [Fuente] nvarchar(max) NULL,
        [Referencia] nvarchar(max) NULL,
        CONSTRAINT [PK_ProductoSolicitudes] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_ProductoSolicitudes_Solicitudes_SolicitudId] FOREIGN KEY ([SolicitudId]) REFERENCES [Solicitudes] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE TABLE [ProductoSolicitudDetalles] (
        [Id] int NOT NULL IDENTITY,
        [ProductoSolicitudId] int NOT NULL,
        [ItemId] int NULL,
        [PlantillaProductoCompletoId] int NOT NULL,
        [Estatus] bit NOT NULL,
        [FechaCreacion] datetime2 NOT NULL,
        [FechaActualizacion] datetime2 NOT NULL,
        [FechaEnsamblaje] datetime2 NOT NULL,
        [UsuarioCreacion] nvarchar(max) NULL,
        [UsuarioActualizacion] nvarchar(max) NULL,
        [Fuente] nvarchar(max) NULL,
        [Referencia] nvarchar(max) NULL,
        CONSTRAINT [PK_ProductoSolicitudDetalles] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_ProductoSolicitudDetalles_Items_ItemId] FOREIGN KEY ([ItemId]) REFERENCES [Items] ([Id]),
        CONSTRAINT [FK_ProductoSolicitudDetalles_PlantillaProductoCompletos_PlantillaProductoCompletoId] FOREIGN KEY ([PlantillaProductoCompletoId]) REFERENCES [PlantillaProductoCompletos] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_ProductoSolicitudDetalles_ProductoSolicitudes_ProductoSolicitudId] FOREIGN KEY ([ProductoSolicitudId]) REFERENCES [ProductoSolicitudes] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE INDEX [IX_AspNetRoleClaims_RoleId] ON [AspNetRoleClaims] ([RoleId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    EXEC(N'CREATE UNIQUE INDEX [RoleNameIndex] ON [AspNetRoles] ([NormalizedName]) WHERE [NormalizedName] IS NOT NULL');
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE INDEX [IX_AspNetUserClaims_UserId] ON [AspNetUserClaims] ([UserId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE INDEX [IX_AspNetUserLogins_UserId] ON [AspNetUserLogins] ([UserId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE INDEX [IX_AspNetUserRoles_RoleId] ON [AspNetUserRoles] ([RoleId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE INDEX [EmailIndex] ON [AspNetUsers] ([NormalizedEmail]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    EXEC(N'CREATE UNIQUE INDEX [UserNameIndex] ON [AspNetUsers] ([NormalizedUserName]) WHERE [NormalizedUserName] IS NOT NULL');
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE INDEX [IX_ItemExistencias_ItemId] ON [ItemExistencias] ([ItemId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE INDEX [IX_Localidades_UsuarioId] ON [Localidades] ([UsuarioId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE INDEX [IX_PlantillaProductoCompletoDetalles_PlantillaProductoCompletoId] ON [PlantillaProductoCompletoDetalles] ([PlantillaProductoCompletoId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE INDEX [IX_ProductoSolicitudDetalles_ItemId] ON [ProductoSolicitudDetalles] ([ItemId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE INDEX [IX_ProductoSolicitudDetalles_PlantillaProductoCompletoId] ON [ProductoSolicitudDetalles] ([PlantillaProductoCompletoId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE INDEX [IX_ProductoSolicitudDetalles_ProductoSolicitudId] ON [ProductoSolicitudDetalles] ([ProductoSolicitudId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    CREATE INDEX [IX_ProductoSolicitudes_SolicitudId] ON [ProductoSolicitudes] ([SolicitudId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202193655_initial')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20221202193655_initial', N'6.0.11');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221203161450_addTablemuchoamucho')
BEGIN
    ALTER TABLE [Localidades] DROP CONSTRAINT [FK_Localidades_AspNetUsers_UsuarioId];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221203161450_addTablemuchoamucho')
BEGIN
    DROP INDEX [IX_Localidades_UsuarioId] ON [Localidades];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221203161450_addTablemuchoamucho')
BEGIN
    DECLARE @var0 sysname;
    SELECT @var0 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Localidades]') AND [c].[name] = N'UsuarioId');
    IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Localidades] DROP CONSTRAINT [' + @var0 + '];');
    ALTER TABLE [Localidades] DROP COLUMN [UsuarioId];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221203161450_addTablemuchoamucho')
BEGIN
    CREATE TABLE [UsuarioLocalidad] (
        [Id] int NOT NULL IDENTITY,
        [UsuarioId] nvarchar(450) NULL,
        [LocalidadId] int NOT NULL,
        CONSTRAINT [PK_UsuarioLocalidad] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_UsuarioLocalidad_AspNetUsers_UsuarioId] FOREIGN KEY ([UsuarioId]) REFERENCES [AspNetUsers] ([Id]),
        CONSTRAINT [FK_UsuarioLocalidad_Localidades_LocalidadId] FOREIGN KEY ([LocalidadId]) REFERENCES [Localidades] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221203161450_addTablemuchoamucho')
BEGIN
    CREATE INDEX [IX_UsuarioLocalidad_LocalidadId] ON [UsuarioLocalidad] ([LocalidadId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221203161450_addTablemuchoamucho')
BEGIN
    CREATE INDEX [IX_UsuarioLocalidad_UsuarioId] ON [UsuarioLocalidad] ([UsuarioId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221203161450_addTablemuchoamucho')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20221203161450_addTablemuchoamucho', N'6.0.11');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221205130828_agregandoCamposLocalidad')
BEGIN
    ALTER TABLE [Localidades] ADD [Latitud] nvarchar(20) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221205130828_agregandoCamposLocalidad')
BEGIN
    ALTER TABLE [Localidades] ADD [Longitud] nvarchar(20) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221205130828_agregandoCamposLocalidad')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20221205130828_agregandoCamposLocalidad', N'6.0.11');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221208234816_addSolicitudProducto')
BEGIN
    ALTER TABLE [ProductoSolicitudDetalles] DROP CONSTRAINT [FK_ProductoSolicitudDetalles_PlantillaProductoCompletos_PlantillaProductoCompletoId];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221208234816_addSolicitudProducto')
BEGIN
    EXEC sp_rename N'[ProductoSolicitudDetalles].[PlantillaProductoCompletoId]', N'ItemParteId', N'COLUMN';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221208234816_addSolicitudProducto')
BEGIN
    EXEC sp_rename N'[ProductoSolicitudDetalles].[IX_ProductoSolicitudDetalles_PlantillaProductoCompletoId]', N'IX_ProductoSolicitudDetalles_ItemParteId', N'INDEX';
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221208234816_addSolicitudProducto')
BEGIN
    ALTER TABLE [ProductoSolicitudes] ADD [PlantillaProductoCompletoId] int NOT NULL DEFAULT 0;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221208234816_addSolicitudProducto')
BEGIN
    CREATE INDEX [IX_ProductoSolicitudes_PlantillaProductoCompletoId] ON [ProductoSolicitudes] ([PlantillaProductoCompletoId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221208234816_addSolicitudProducto')
BEGIN
    ALTER TABLE [ProductoSolicitudDetalles] ADD CONSTRAINT [FK_ProductoSolicitudDetalles_ItemPartes_ItemParteId] FOREIGN KEY ([ItemParteId]) REFERENCES [ItemPartes] ([Id]) ON DELETE CASCADE;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221208234816_addSolicitudProducto')
BEGIN
    ALTER TABLE [ProductoSolicitudes] ADD CONSTRAINT [FK_ProductoSolicitudes_PlantillaProductoCompletos_PlantillaProductoCompletoId] FOREIGN KEY ([PlantillaProductoCompletoId]) REFERENCES [PlantillaProductoCompletos] ([Id]) ON DELETE CASCADE;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221208234816_addSolicitudProducto')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20221208234816_addSolicitudProducto', N'6.0.11');
END;
GO

COMMIT;
GO

