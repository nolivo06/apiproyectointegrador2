﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE TABLE [AspNetRoles] (
        [Id] nvarchar(450) NOT NULL,
        [Name] nvarchar(256) NULL,
        [NormalizedName] nvarchar(256) NULL,
        [ConcurrencyStamp] nvarchar(max) NULL,
        CONSTRAINT [PK_AspNetRoles] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE TABLE [AspNetUsers] (
        [Id] nvarchar(450) NOT NULL,
        [UserName] nvarchar(256) NULL,
        [NormalizedUserName] nvarchar(256) NULL,
        [Email] nvarchar(256) NULL,
        [NormalizedEmail] nvarchar(256) NULL,
        [EmailConfirmed] bit NOT NULL,
        [PasswordHash] nvarchar(max) NULL,
        [SecurityStamp] nvarchar(max) NULL,
        [ConcurrencyStamp] nvarchar(max) NULL,
        [PhoneNumber] nvarchar(max) NULL,
        [PhoneNumberConfirmed] bit NOT NULL,
        [TwoFactorEnabled] bit NOT NULL,
        [LockoutEnd] datetimeoffset NULL,
        [LockoutEnabled] bit NOT NULL,
        [AccessFailedCount] int NOT NULL,
        CONSTRAINT [PK_AspNetUsers] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE TABLE [ItemPartes] (
        [Id] int NOT NULL IDENTITY,
        [Nombre] nvarchar(max) NULL,
        [Tipo] nvarchar(max) NULL,
        [Estatus] bit NOT NULL,
        [FechaCreacion] datetime2 NOT NULL,
        [FechaActualizacion] datetime2 NOT NULL,
        [UsuarioCreacion] nvarchar(max) NULL,
        [UsuarioActualizacion] nvarchar(max) NULL,
        CONSTRAINT [PK_ItemPartes] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE TABLE [Items] (
        [Id] int NOT NULL IDENTITY,
        [Nombre] nvarchar(max) NULL,
        [Tipo] nvarchar(max) NULL,
        [Estatus] bit NOT NULL,
        [FechaCreacion] datetime2 NOT NULL,
        [FechaActualizacion] datetime2 NOT NULL,
        [UsuarioCreacion] nvarchar(max) NULL,
        [UsuarioActualizacion] nvarchar(max) NULL,
        [Fuente] nvarchar(max) NULL,
        [Referencia] nvarchar(max) NULL,
        CONSTRAINT [PK_Items] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE TABLE [PlantillaProductoCompletos] (
        [Id] int NOT NULL IDENTITY,
        [Nombre] nvarchar(max) NULL,
        [Descripcion] nvarchar(max) NULL,
        [Estatus] bit NOT NULL,
        [FechaCreacion] datetime2 NOT NULL,
        [FechaActualizacion] datetime2 NOT NULL,
        [UsuarioCreacion] nvarchar(max) NULL,
        [UsuarioActualizacion] nvarchar(max) NULL,
        CONSTRAINT [PK_PlantillaProductoCompletos] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE TABLE [Solicitudes] (
        [Id] int NOT NULL IDENTITY,
        [Nombre] nvarchar(max) NULL,
        [Descripcion] nvarchar(max) NULL,
        [Estatus] bit NOT NULL,
        [FechaCreacion] datetime2 NOT NULL,
        [FechaActualizacion] datetime2 NOT NULL,
        [UsuarioCreacion] nvarchar(max) NULL,
        [UsuarioActualizacion] nvarchar(max) NULL,
        CONSTRAINT [PK_Solicitudes] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE TABLE [AspNetRoleClaims] (
        [Id] int NOT NULL IDENTITY,
        [RoleId] nvarchar(450) NOT NULL,
        [ClaimType] nvarchar(max) NULL,
        [ClaimValue] nvarchar(max) NULL,
        CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE TABLE [AspNetUserClaims] (
        [Id] int NOT NULL IDENTITY,
        [UserId] nvarchar(450) NOT NULL,
        [ClaimType] nvarchar(max) NULL,
        [ClaimValue] nvarchar(max) NULL,
        CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE TABLE [AspNetUserLogins] (
        [LoginProvider] nvarchar(450) NOT NULL,
        [ProviderKey] nvarchar(450) NOT NULL,
        [ProviderDisplayName] nvarchar(max) NULL,
        [UserId] nvarchar(450) NOT NULL,
        CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY ([LoginProvider], [ProviderKey]),
        CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE TABLE [AspNetUserRoles] (
        [UserId] nvarchar(450) NOT NULL,
        [RoleId] nvarchar(450) NOT NULL,
        CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY ([UserId], [RoleId]),
        CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE TABLE [AspNetUserTokens] (
        [UserId] nvarchar(450) NOT NULL,
        [LoginProvider] nvarchar(450) NOT NULL,
        [Name] nvarchar(450) NOT NULL,
        [Value] nvarchar(max) NULL,
        CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY ([UserId], [LoginProvider], [Name]),
        CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE TABLE [Localidades] (
        [Id] int NOT NULL IDENTITY,
        [Nombre] nvarchar(max) NULL,
        [Estatus] nvarchar(max) NULL,
        [FechaCreacion] datetime2 NOT NULL,
        [FechaActualizacion] datetime2 NOT NULL,
        [UsuarioCreacion] nvarchar(max) NULL,
        [UsuarioActualizacion] nvarchar(max) NULL,
        [UsuarioId] nvarchar(450) NULL,
        CONSTRAINT [PK_Localidades] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Localidades_AspNetUsers_UsuarioId] FOREIGN KEY ([UsuarioId]) REFERENCES [AspNetUsers] ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE TABLE [ItemExistencias] (
        [Id] int NOT NULL IDENTITY,
        [ItemId] int NOT NULL,
        [Lote] nvarchar(max) NULL,
        [Cantidad] int NOT NULL,
        [FechaRealizada] datetime2 NOT NULL,
        [FechaCreacion] datetime2 NOT NULL,
        [FechaActualizacion] datetime2 NOT NULL,
        CONSTRAINT [PK_ItemExistencias] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_ItemExistencias_Items_ItemId] FOREIGN KEY ([ItemId]) REFERENCES [Items] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE TABLE [PlantillaProductoCompletoDetalles] (
        [ItemParteId] int NOT NULL,
        [PlantillaProductoCompletoId] int NOT NULL,
        [FechaCreacion] datetime2 NOT NULL,
        [FechaActualizacion] datetime2 NOT NULL,
        [UsuarioCreacion] nvarchar(max) NULL,
        [UsuarioActualizacion] nvarchar(max) NULL,
        CONSTRAINT [PK_PlantillaProductoCompletoDetalles] PRIMARY KEY ([ItemParteId], [PlantillaProductoCompletoId]),
        CONSTRAINT [FK_PlantillaProductoCompletoDetalles_ItemPartes_ItemParteId] FOREIGN KEY ([ItemParteId]) REFERENCES [ItemPartes] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_PlantillaProductoCompletoDetalles_PlantillaProductoCompletos_PlantillaProductoCompletoId] FOREIGN KEY ([PlantillaProductoCompletoId]) REFERENCES [PlantillaProductoCompletos] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE TABLE [ProductoSolicitudes] (
        [Id] int NOT NULL IDENTITY,
        [SolicitudId] int NOT NULL,
        [Nombre] nvarchar(max) NULL,
        [Descripcion] nvarchar(max) NULL,
        [Estatus] bit NOT NULL,
        [FechaCreacion] datetime2 NOT NULL,
        [FechaActualizacion] datetime2 NOT NULL,
        [UsuarioCreacion] nvarchar(max) NULL,
        [UsuarioActualizacion] nvarchar(max) NULL,
        [Fuente] nvarchar(max) NULL,
        [Referencia] nvarchar(max) NULL,
        CONSTRAINT [PK_ProductoSolicitudes] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_ProductoSolicitudes_Solicitudes_SolicitudId] FOREIGN KEY ([SolicitudId]) REFERENCES [Solicitudes] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE TABLE [UsuarioLocalidad] (
        [UsuarioId] nvarchar(450) NOT NULL,
        [LocalidadId] int NOT NULL,
        CONSTRAINT [PK_UsuarioLocalidad] PRIMARY KEY ([LocalidadId], [UsuarioId]),
        CONSTRAINT [FK_UsuarioLocalidad_AspNetUsers_UsuarioId] FOREIGN KEY ([UsuarioId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_UsuarioLocalidad_Localidades_LocalidadId] FOREIGN KEY ([LocalidadId]) REFERENCES [Localidades] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE TABLE [ProductoSolicitudDetalles] (
        [ProductoSolicitudId] int NOT NULL,
        [ItemId] int NOT NULL,
        [PlantillaProductoCompletoId] int NOT NULL,
        [Estatus] bit NOT NULL,
        [FechaCreacion] datetime2 NOT NULL,
        [FechaActualizacion] datetime2 NOT NULL,
        [FechaEnsamblaje] datetime2 NOT NULL,
        [UsuarioCreacion] nvarchar(max) NULL,
        [UsuarioActualizacion] nvarchar(max) NULL,
        [Fuente] nvarchar(max) NULL,
        [Referencia] nvarchar(max) NULL,
        CONSTRAINT [PK_ProductoSolicitudDetalles] PRIMARY KEY ([ItemId], [ProductoSolicitudId], [PlantillaProductoCompletoId]),
        CONSTRAINT [FK_ProductoSolicitudDetalles_Items_ItemId] FOREIGN KEY ([ItemId]) REFERENCES [Items] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_ProductoSolicitudDetalles_PlantillaProductoCompletos_PlantillaProductoCompletoId] FOREIGN KEY ([PlantillaProductoCompletoId]) REFERENCES [PlantillaProductoCompletos] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_ProductoSolicitudDetalles_ProductoSolicitudes_ProductoSolicitudId] FOREIGN KEY ([ProductoSolicitudId]) REFERENCES [ProductoSolicitudes] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE INDEX [IX_AspNetRoleClaims_RoleId] ON [AspNetRoleClaims] ([RoleId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    EXEC(N'CREATE UNIQUE INDEX [RoleNameIndex] ON [AspNetRoles] ([NormalizedName]) WHERE [NormalizedName] IS NOT NULL');
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE INDEX [IX_AspNetUserClaims_UserId] ON [AspNetUserClaims] ([UserId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE INDEX [IX_AspNetUserLogins_UserId] ON [AspNetUserLogins] ([UserId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE INDEX [IX_AspNetUserRoles_RoleId] ON [AspNetUserRoles] ([RoleId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE INDEX [EmailIndex] ON [AspNetUsers] ([NormalizedEmail]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    EXEC(N'CREATE UNIQUE INDEX [UserNameIndex] ON [AspNetUsers] ([NormalizedUserName]) WHERE [NormalizedUserName] IS NOT NULL');
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE INDEX [IX_ItemExistencias_ItemId] ON [ItemExistencias] ([ItemId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE INDEX [IX_Localidades_UsuarioId] ON [Localidades] ([UsuarioId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE INDEX [IX_PlantillaProductoCompletoDetalles_PlantillaProductoCompletoId] ON [PlantillaProductoCompletoDetalles] ([PlantillaProductoCompletoId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE INDEX [IX_ProductoSolicitudDetalles_PlantillaProductoCompletoId] ON [ProductoSolicitudDetalles] ([PlantillaProductoCompletoId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE INDEX [IX_ProductoSolicitudDetalles_ProductoSolicitudId] ON [ProductoSolicitudDetalles] ([ProductoSolicitudId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE INDEX [IX_ProductoSolicitudes_SolicitudId] ON [ProductoSolicitudes] ([SolicitudId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    CREATE INDEX [IX_UsuarioLocalidad_UsuarioId] ON [UsuarioLocalidad] ([UsuarioId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221129201534_Initial')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20221129201534_Initial', N'6.0.11');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221130024247_cambiandoClavePrimaria')
BEGIN
    ALTER TABLE [ProductoSolicitudDetalles] DROP CONSTRAINT [PK_ProductoSolicitudDetalles];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221130024247_cambiandoClavePrimaria')
BEGIN
    DROP INDEX [IX_ProductoSolicitudDetalles_ProductoSolicitudId] ON [ProductoSolicitudDetalles];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221130024247_cambiandoClavePrimaria')
BEGIN
    ALTER TABLE [ProductoSolicitudDetalles] ADD CONSTRAINT [PK_ProductoSolicitudDetalles] PRIMARY KEY ([ProductoSolicitudId], [PlantillaProductoCompletoId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221130024247_cambiandoClavePrimaria')
BEGIN
    CREATE INDEX [IX_ProductoSolicitudDetalles_ItemId] ON [ProductoSolicitudDetalles] ([ItemId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221130024247_cambiandoClavePrimaria')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20221130024247_cambiandoClavePrimaria', N'6.0.11');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221130025101_agregandoId')
BEGIN
    ALTER TABLE [ProductoSolicitudDetalles] DROP CONSTRAINT [PK_ProductoSolicitudDetalles];
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221130025101_agregandoId')
BEGIN
    ALTER TABLE [ProductoSolicitudDetalles] ADD [Id] int NOT NULL IDENTITY;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221130025101_agregandoId')
BEGIN
    ALTER TABLE [ProductoSolicitudDetalles] ADD CONSTRAINT [PK_ProductoSolicitudDetalles] PRIMARY KEY ([Id]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221130025101_agregandoId')
BEGIN
    CREATE INDEX [IX_ProductoSolicitudDetalles_ProductoSolicitudId] ON [ProductoSolicitudDetalles] ([ProductoSolicitudId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221130025101_agregandoId')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20221130025101_agregandoId', N'6.0.11');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202181022_agregandoUnoAMucho')
BEGIN
    ALTER TABLE [Localidades] ADD [UsuarioId] nvarchar(450) NULL;
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202181022_agregandoUnoAMucho')
BEGIN
    CREATE INDEX [IX_Localidades_UsuarioId] ON [Localidades] ([UsuarioId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202181022_agregandoUnoAMucho')
BEGIN
    ALTER TABLE [Localidades] ADD CONSTRAINT [FK_Localidades_AspNetUsers_UsuarioId] FOREIGN KEY ([UsuarioId]) REFERENCES [AspNetUsers] ([Id]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20221202181022_agregandoUnoAMucho')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20221202181022_agregandoUnoAMucho', N'6.0.11');
END;
GO

COMMIT;
GO

