using APIProyectoIntegrador;

var builder = WebApplication.CreateBuilder(args);
var staturp = new Startup(builder.Configuration);

staturp.ConfigureServices(builder.Services);
var app = builder.Build();

staturp.Configure(app, app.Environment);
app.Run();
