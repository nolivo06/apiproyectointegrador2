﻿namespace APIProyectoIntegrador.DTO.DTOSolicitudes
{
    public class CreacionSolicitudDTO
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}
