﻿namespace APIProyectoIntegrador.DTO.DTOSolicitudes
{
    public class SolicitudDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}
