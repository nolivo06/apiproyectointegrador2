﻿using System.ComponentModel.DataAnnotations;

namespace APIProyectoIntegrador.DTO.DtoUsuario
{
    public class CredencialesUsuario
    {
        //public string Id { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Clave { get; set; }
    }
}
