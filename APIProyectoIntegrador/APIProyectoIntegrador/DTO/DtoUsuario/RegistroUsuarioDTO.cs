﻿using System.ComponentModel.DataAnnotations;

namespace APIProyectoIntegrador.DTO.DtoUsuario
{
    public class RegistroUsuarioDTO
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Clave { get; set; }

    }
}
