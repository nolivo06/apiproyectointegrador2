﻿using System.ComponentModel.DataAnnotations;

namespace APIProyectoIntegrador.DTO.DTOUsuario
{
    public class CambiarClaveDTO
    {
        [Required(ErrorMessage = "Es requerido una direccion de correo valida")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage ="Se requiere colocar la clave anterior")]
        public string ClaveAnterior { get; set; }

        [Required(ErrorMessage = "Se requiere una nueva clave")]
        public string ClaveNueva { get; set; }

        [Required(ErrorMessage = "Confirmar la nueva clave es requerida")]
        public string ConfirmarClaveNueva { get; set; }
    }
}
