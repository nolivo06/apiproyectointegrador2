﻿namespace APIProyectoIntegrador.DTO.DtoUsuario
{
    public class EmailDTO
    {
        public string? Id { get; set; }
        public string Email { get; set; }
    }
}
