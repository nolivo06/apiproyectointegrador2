﻿namespace APIProyectoIntegrador.DTO.DtoUsuario
{
    public class RespuestaAutenticacion
    {
        public string Token { get; set; }
        public DateTime Expiracion { get; set; }
    }
}
