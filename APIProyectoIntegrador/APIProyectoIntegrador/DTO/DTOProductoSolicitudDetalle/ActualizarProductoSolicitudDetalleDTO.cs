﻿using System.ComponentModel.DataAnnotations;

namespace APIProyectoIntegrador.DTO.DTOProductoSolicitudDetalle
{
    public class ActualizarProductoSolicitudDetalleDTO
    {
        [Required]
        public string Referencia { get; set; }
    }
}
