﻿using System.ComponentModel.DataAnnotations;

namespace APIProyectoIntegrador.DTO.DTOProductoSolicitudDetalle
{
    public class CreacionProductoSolicitudDetalleDTO
    {
        [Required]
        public string Referencia { get; set; }
    }
}
