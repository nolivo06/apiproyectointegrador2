﻿namespace APIProyectoIntegrador.DTO.DTOProductoSolicitudDetalle
{
    public class ProductoSolicitudDetalleDTO
    {
        public int Id { get; set; }
        public int ProductoSolicitudId { get; set; }
        //public int ItemId { get; set; }
        public int itemParteId { get; set; }
        public bool Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaEnsamblaje { get; set; }
        public string Fuente { get; set; }
        public string Referencia { get; set; }
    }
}
