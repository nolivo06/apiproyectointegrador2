﻿namespace APIProyectoIntegrador.DTO.DTOItem
{
    public class CreacionItemDTO
    {
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public string Fuente { get; set; }
        public string Referencia { get; set; }
    }
}
