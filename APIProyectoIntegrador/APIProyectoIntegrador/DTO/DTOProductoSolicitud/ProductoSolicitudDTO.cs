﻿namespace APIProyectoIntegrador.DTO.DTOProductoSolicitud
{
    public class ProductoSolicitudDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string Fuente { get; set; }
        public string Referencia { get; set; }
    }
}
