﻿using System.ComponentModel.DataAnnotations;

namespace APIProyectoIntegrador.DTO.DTOProductoSolicitud
{
    public class CreacionProductoSolicitudDTO
    {
        
        //public string Nombre { get; set; }
        [Required]
        public string Descripcion { get; set; }
        public string Fuente { get; set; }
        public string Referencia { get; set; }
    }
}
