﻿namespace APIProyectoIntegrador.DTO.DTOItemExistencia
{
    public class ItemExistenciaDTO
    {
        public int Id { get; set; }
        public string Lote { get; set; }
        public int Cantidad { get; set; }
        public DateTime FechaRealizada { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}
