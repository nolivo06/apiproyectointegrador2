﻿using System.ComponentModel.DataAnnotations;

namespace APIProyectoIntegrador.DTO.DTOItemExistencia
{
    public class CreacionItemExistenciaDTO
    {
        [Required]
        public string Lote { get; set; }
        [Required]
        public int Cantidad { get; set; }
    }
}
