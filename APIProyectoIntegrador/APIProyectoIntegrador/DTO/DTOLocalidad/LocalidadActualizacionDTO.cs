﻿using System.ComponentModel.DataAnnotations;

namespace APIProyectoIntegrador.DTO.Localidad
{
    public class LocalidadActualizacionDTO
    {
        [Required]
        public string Nombre { get; set; }
        [Required]
        [StringLength(20)]
        public string Latitud { get; set; }
        [Required]
        [StringLength(20)]
        public string Longitud { get; set; }

    }
}
