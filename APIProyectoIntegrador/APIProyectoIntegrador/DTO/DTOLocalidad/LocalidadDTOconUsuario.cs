﻿using APIProyectoIntegrador.DTO.DtoUsuario;
using APIProyectoIntegrador.DTO.Localidad;
using Microsoft.AspNetCore.Identity;
using APIProyectoIntegrador.Models;

namespace APIProyectoIntegrador.DTO.DTOLocalidad
{
    public class LocalidadDTOconUsuario:LocalidadDTO
    {
        public List<UsuariosDTO> Usuarios { get; set; }
        
    }
}
