﻿using System.ComponentModel.DataAnnotations;

namespace APIProyectoIntegrador.DTO.Localidad
{
    public class LocalidadDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
    }
}
