﻿using System.ComponentModel.DataAnnotations;

namespace APIProyectoIntegrador.DTO.DTOItemPartes
{
    public class ItemParteCreacionDTO
    {
        [Required(ErrorMessage ="El campo {0} es requerido")]
        [StringLength(maximumLength:120, ErrorMessage =" El campo {0} no debe de tener mas de {1}")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(maximumLength: 100, ErrorMessage = " El campo {0} no debe de tener mas de {1}")]
        public string Tipo { get; set; }

        
    }
}
