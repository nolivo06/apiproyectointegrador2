﻿using APIProyectoIntegrador.DTO.DTOProductoCompleto;

namespace APIProyectoIntegrador.DTO.ItemPartes
{
    public class ItemsDTOconProductos:ItemParteDTO
    {
        public List<ProductoCompletosDTO> ProductoCompletos { get; set; }
    }
}
