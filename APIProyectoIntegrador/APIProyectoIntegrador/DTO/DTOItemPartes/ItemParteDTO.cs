﻿namespace APIProyectoIntegrador.DTO.ItemPartes
{
    public class ItemParteDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public bool Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}
