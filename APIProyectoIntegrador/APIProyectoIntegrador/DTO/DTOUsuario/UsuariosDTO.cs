﻿namespace APIProyectoIntegrador.DTO.DtoUsuario
{
    public class UsuariosDTO
    {
        public string Id { get; set; }
        public string Email { get; set; }
    }
}
