﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace APIProyectoIntegrador.DTO.DtoUsuario
{
    public class CambiarClaveOlvidadaDTO
    {

        [Required(ErrorMessage ="Es requerido una direccion de correo valida")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage ="Se requiere una nueva clave")]   
        public string ClaveNueva { get; set; }

        [Required(ErrorMessage ="Confirmar la nueva clave es requerida")]
        public string ConfirmarClaveNueva { get; set; }

        //public string Token { get; set; }
    }
}
