﻿using System.ComponentModel.DataAnnotations;

namespace APIProyectoIntegrador.DTO.DTOProductoCompleto
{
    public class ProductoCompletoCreacionDTO
    {
        [Required(ErrorMessage ="El campo {0} es requerido")]
        [StringLength(maximumLength: 120, ErrorMessage = " El campo {0} no debe de tener mas de {1}")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(maximumLength: 120, ErrorMessage = " El campo {0} no debe de tener mas de {1}")]
        public string Descripcion { get; set; }

        public List<int> ItemParte { get; set; }
    }
}
