﻿using APIProyectoIntegrador.DTO.ItemPartes;

namespace APIProyectoIntegrador.DTO.DTOProductoCompleto
{
    public class ProductosDTOconItems:ProductoCompletosDTO
    {
        public List<ItemParteDTO> ItemParte { get; set; }
    }
}
